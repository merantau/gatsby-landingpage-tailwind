# [GatsbyJS Tailwind Starter](https://gatsbyjs-starter-tailwindplay.appseed.us/)

<br >

[https://gatsbyjs-starter-tailwindplay.appseed.us/](https://gatsbyjs-starter-tailwindplay.appseed.us/)

<br />

![Gatsby Tailwind Starter - Gif animated intro.](https://raw.githubusercontent.com/app-generator/static/master/products/gatsbyjs-starter-tailwindplay-intro.gif)

<br />

## Tailwind?
>"Tailwind is a utility-first CSS framework for rapidly building custom user interfaces."
–[Tailwind](https://tailwindcss.com)

<br />

## Get started

Install Gatsby CLI:
```sh
npm i --global gatsby-cli
```

Create new Gatsby project using this starter:
```sh
gatsby new my-new-website https://github.com/app-generator/gatsbyjs-starter-tailwindplay
```

Build stylesheet from Tailwind config and run the project in development mode:
```sh
cd my-new-website
npm run develop
```

<br />

## Format and lint
* `npm run analyze` - See what ESLint and Prettier can fix
* `npm run fix` - Run Prettier and ESLint with the `--fix` option

<br />

## Build your site
Use `npm run build` to build your site for production.

<br />

## Deploy

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/app-generator/gatsbyjs-starter-tailwindplay)

<br />

---
### Thank you, @merantau